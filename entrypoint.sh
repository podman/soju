#!/bin/bash

# setup defaults
PORT="${PORT:-6667}"
SOJU_HOST="${SOJU_HOST:-${HOSTNAME}}"

if [[ ! -f /config/soju.cfg ]]; then
    (
        echo "listen irc+insecure://0.0.0.0:${PORT}"
        echo "hostname ${SOJU_HOST}"
        echo "db sqlite3 /config/soju.db"
    ) > /config/soju.cfg
fi

grep '^user ' <<< "${SOJU_INIT}" | while read -r _ user password admin; do
    echo "${password}" | /app/sojuctl -config /config/soju.cfg create-user "${user}" "${admin:+-admin}"
done

exec /app/soju -config /config/soju.cfg
