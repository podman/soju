FROM docker.io/golang:alpine AS build-env
WORKDIR /build

ARG GIT_TAG="v0.4.0"

RUN apk add --no-cache gcc libc-dev git

RUN git clone https://github.com/emersion/soju.git . && \
    git checkout ${GIT_TAG}

RUN go build -o /build/app/soju cmd/soju/main.go && \
    go build -o /build/app/sojuctl cmd/sojuctl/main.go

FROM alpine:latest

RUN apk add --no-cache bash ca-certificates openssl && \
    mkdir /config

COPY --from=build-env /build/app/ /app/
COPY entrypoint.sh /app/

WORKDIR /app

CMD ["/app/entrypoint.sh"]
